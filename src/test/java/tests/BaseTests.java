package tests;

import com.beust.jcommander.Parameter;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import listeners.Listener;
import org.testng.annotations.*;

@Listeners(Listener.class)
public class BaseTests {

    @Parameters({"browser"})
    @BeforeClass
    public void setUp(String browserName) {
        if (browserName.equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            Configuration.browser = "chrome";
        }
        if (browserName.equals("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            Configuration.browser = "firefox";
        }
        Configuration.browserSize = "1920x1080";
        Configuration.pageLoadStrategy = "none";
        //        Configuration.headless = true;
    }

    @AfterMethod
    public void tearDown() {
        Selenide.closeWebDriver();
    }

}
