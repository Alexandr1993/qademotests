package tests;

import constants.ConstantsElements;
import org.testng.annotations.Test;
import page.FieldEements;
import page.MainPage;

import java.util.HashMap;

public class ElementsTests extends BaseTests{
    @Test
    public void checkTest() {
        HashMap<FieldEements, String> expectedResults = new HashMap<>();
        expectedResults.put(FieldEements.FULL_NAME, "Alexandr");
        expectedResults.put(FieldEements.EMAIL, "alexandr@gmail.com");
        expectedResults.put(FieldEements.CURRENT_ADDRESS, "Moskva");
        expectedResults.put(FieldEements.PERMANENT_ADDRESS, "Tretyakov");

        new MainPage()
                .clickElements()
                .clickTextBox(ConstantsElements.TEXT_BOX)
                .setTextFullName("Alexandr")
                .setTextEmail("alexandr@gmail.com")
                .setTextcurrentAddress("Moskva")
                .setTextpermanentAddress("Tretyakov")
                .clickSubmit()
                .checkResultField(expectedResults);
    }

}
