package page;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;

public class MainPage extends Page{
    private static final String titleElements = "//h5[text()=\'Elements\']";
    public MainPage() {
        open("https://demoqa.com/");
    }

    @Step("Нажать на Elements")
    public ElementsPage clickElements() {
        waitForElement($x(titleElements)).click();
        return page(ElementsPage.class);
    }


}
