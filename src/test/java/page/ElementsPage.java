package page;

import com.codeborne.selenide.Condition;
import constants.ConstantsElements;
import io.qameta.allure.Step;
import org.testng.Assert;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class ElementsPage extends Page{
    private static final String titleTextBox = "//span[text()='{SUBSTRING}']";
    private static final String fullName = "//input[@placeholder='Full Name']";
    private static final String email = "//input[@id='userEmail']";
    private static final String currentAddress = "//textarea[@id='currentAddress']";
    private static final String permanentAddress = "//textarea[@id='permanentAddress']";
    private static final String enterFields = "//p[@class='mb-1']";
    private static final String buttonSubmit = "//button[@id='submit']";
    @Step("Проверить результаты введенных данных")
    public void checkResultField(HashMap expectedResults) {
        Map<FieldEements, String> actualResults = new HashMap<>();
        actualResults.put(FieldEements.FULL_NAME, $$x(enterFields).get(0).getText().split(":", 2)[1]);
        actualResults.put(FieldEements.EMAIL, $$x(enterFields).get(1).getText().split(":", 2)[1]);
        actualResults.put(FieldEements.CURRENT_ADDRESS, $$x(enterFields).get(2).getText().split(":", 2)[1]);
        actualResults.put(FieldEements.PERMANENT_ADDRESS, $$x(enterFields).get(3).getText().split(":", 2)[1]);
        Assert.assertEquals(actualResults, expectedResults);
    }

    @Step("Нажать на {text}")
    public ElementsPage clickTextBox(ConstantsElements text) {
        waitForElement($x(replaceText(titleTextBox, text))).click();
        return this;
    }

    @Step("Ввести в поле Full Name и проверить, что текст введен")
    public ElementsPage setTextFullName(String name) {
        setText(name, fullName).shouldBe(Condition.value(name));
        return this;
    }

    @Step("Ввести в поле Email и проверить, что текст введен")
    public ElementsPage setTextEmail(String name) {
        setText(name, email).shouldBe(Condition.value(name));
        return this;
    }

    @Step("Ввести в поле Email и проверить, что текст введен")
    public ElementsPage setTextcurrentAddress(String name) {
        setText(name, currentAddress).shouldBe(Condition.value(name));
        return this;
    }

    @Step("Ввести в поле Email и проверить, что текст введен")
    public ElementsPage setTextpermanentAddress(String name) {
        setText(name, permanentAddress).shouldBe(Condition.value(name));
        return this;
    }

    @Step("Нажать кнопку Submit")
    public ElementsPage clickSubmit() {
        waitForElement($x(buttonSubmit)).click();
        return this;
    }

}
