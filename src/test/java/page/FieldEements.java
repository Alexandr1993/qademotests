package page;

public enum FieldEements {
    FULL_NAME,
    EMAIL,
    CURRENT_ADDRESS,
    PERMANENT_ADDRESS;
}
