package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import constants.ConstantsElements;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$x;

public class Page {
    protected SelenideElement waitForElement(SelenideElement element, int time) {
        return element.shouldBe(Condition.visible, Duration.ofSeconds(time));
    }

    protected SelenideElement waitForElement(SelenideElement element) {
        return waitForElement(element, 5);
    }

    protected String replaceText(String element, ConstantsElements name) {
        String text = null;
        switch (name) {
            case TEXT_BOX -> text = "Text Box";
            default -> text = "";
        }
        return element.replace("{SUBSTRING}", text);
    }

    protected SelenideElement setText(String name, String element) {
        return waitForElement($x(element)).setValue(name).shouldBe(Condition.value(name));
    }
}
